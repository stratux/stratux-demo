{ nixpkgs ? import <nixpkgs> {}, compiler ? "default" }:

let

  inherit (nixpkgs) pkgs;

  haskellPackages = if compiler == "default"
                       then pkgs.haskellPackages
                       else pkgs.haskell.packages.${compiler};

  sources = {
    papa = pkgs.fetchFromGitHub {
      owner = "qfpl";
      repo = "papa";
      rev = "97ef00aa45c70213a4f0ce348a2208e3f482a7e3";
      sha256 = "0qm0ay49wc0frxs6ipc10xyjj654b0wgk0b1hzm79qdlfp2yq0n5";
    };

    stratux = pkgs.fetchFromGitLab {
      owner = "stratux";
      repo = "stratux";
      rev = "daa47ee0caa7871bccbcc0d69551807d7b57ce8c";
      sha256 = "0b51iypb8jyz7davsr7avyn90na95di66l8jpbhvk4vc2c4gffxx";
    };

  };

  modifiedHaskellPackages = haskellPackages.override {
    overrides = self: super: import sources.papa self // {
      stratux = import sources.stratux {};
      parsers = pkgs.haskell.lib.dontCheck super.parsers;        
    };
  };

  stratux-demo = modifiedHaskellPackages.callPackage ./stratux-demo.nix {};

in

  stratux-demo
