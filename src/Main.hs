{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE CPP #-}

module Main(
  main
) where

import Data.Aviation.Stratux.Demo(run)
import System.IO(IO)

main ::
  IO ()
main =
  run
