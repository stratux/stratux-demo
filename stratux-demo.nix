{ mkDerivation, base, lens, network-uri, optparse-applicative
, stdenv, stratux, text, time, transformers
}:
mkDerivation {
  pname = "stratux-demo";
  version = "0.0.12";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    base lens network-uri optparse-applicative stratux text time
    transformers
  ];
  executableHaskellDepends = [
    base lens network-uri optparse-applicative stratux text time
    transformers
  ];
  homepage = "https://gitlab.com/stratux/stratux-demo";
  description = "A demonstration of the stratux library";
  license = stdenv.lib.licenses.bsd3;
}
